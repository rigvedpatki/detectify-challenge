import Vue from 'vue'
import Router from 'vue-router'
import HomePage from './views/Home.vue'
import TablePage from './views/Table.vue'
import ErrorPage from './views/404.vue'
import StatisticsPage from './views/Statistics.vue'
import SearchPage from './views/Search.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'homePage',
      component: HomePage
    },
    {
      path: '/table/:tableId',
      name: 'tableDataPage',
      component: TablePage
    },
    {
      path: '/statistics',
      name: 'staticsPage',
      component: StatisticsPage
    },
    {
      path: '/search',
      name: 'searchPage',
      component: SearchPage
    },
    {
      path: '/:tableType/:tableOrientation/:headerPosition',
      name: 'resultPage',
      component: HomePage
    },
    {
      path: '*',
      name: 'ErrorPage',
      component: ErrorPage
    }
  ]
})
