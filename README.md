# Detectify Challenge Solution

The solution is divided into two parts:

1.  Front-end : SPA developed using Vuejs
2.  Back-end : Dummuy REST API using json-server

Both these parts are hosted on Heorku.

## Front-end :

Vuejs is used as a basic frontend framework along with Vuetify to give it a Material Design look and feel. Axios is used to communication between the front-end and the back-end. Vue-Router is used for routing between varios pages. Vue-chartjs is used to display statics in the form of doughnut charts. Lodash is used for manipulating arrays in order to display tables.
link :- [Front-end](https://detectify-frontend.herokuapp.com)

## Back-end :

Sample file obtained from WDC website was manipulated as required form JSON file (db.json) with a unique identifier. This manipulation was done using regular expressions and javascript (the code for which is not in this repo). Finally using json-server npm package I was able to create a dummy REST API (no security is implemented for it yet).
link :- [Back-end](https://detectify-challenge-server.herokuapp.com)
